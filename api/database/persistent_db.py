import sqlite3
import logging

class Persistent_db(object):
    """
    This class is a wrapper of sqlite
    """

    def __init__(self, database='weather.db'):
        try:
            self.database = database
            self.connect()

        except Exception as e:
            logging.error("error starting the database")
            logging.error(e)

    def connect(self):
        """Connect to the SQLite3 database."""

        self.db_conn = sqlite3.connect(self.database)
        self.connected = True

    def execute(self, query, params):
        try:
            if not self.connected:
                self.connect()

            self.db_conn.execute(query, params)
            self.db_conn.commit()
        except Exception as e:
            logging.error("error on execute a query")
            logging.error(e)

    def dict_factory(self, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def query(self, query, params, isFetchAll = False):
        try:
            if not self.connected:
                self.connect()

            self.db_conn.row_factory = self.dict_factory
            cur = self.db_conn.cursor()
            cur.execute(query, params)

            if isFetchAll:
                return cur.fetchall()
            else:
                return cur.fetchone()
        except Exception as e:
            logging.error("error on execute a select query")
            logging.error(e)


    def close(self):
        self.db_conn.close()
        self.connected = False


