CREATE TABLE IF NOT EXISTS historical_data (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    search TEXT,
    creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    latitude REAL,
    longitude REAL,
    temp REAL,
    location TEXT
);

select * from historical_data limit 100;