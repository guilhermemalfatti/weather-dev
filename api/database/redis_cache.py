import redis

#TODO: use redis cache instead of database as a cache
class RedisCache():
    """
    This class is a wraper of redis DB
    """

    def __init__(self):
        self.db = redis.StrictRedis(host='localhost', port=6379, db=0, decode_responses=True)

    def get(self, key, default="0"):
        return self.db.get(key) or default

    def set(self, key, value):
        self.db.set(key, value)
