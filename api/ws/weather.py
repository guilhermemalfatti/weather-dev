import requests
import json
import logging

#api.openweathermap.org/data/2.5/weather?lat=-29.2250391&lon=-51.3481054&APPID=b9230b3ff0ca27c2d6272d3e5aa62b37&units=metric
def get_weather(location):
    APPID = 'b9230b3ff0ca27c2d6272d3e5aa62b37'
    OPENWEATHERMAP_URL = 'http://api.openweathermap.org/data/2.5/weather'

    logging.info("get weather call.")
    logging.info(location)

    try:
        params = {
            'lat': location['Latitude'],
            'lon': location['Longitude'],
            'APPID': APPID,
            'units': 'metric'
        }

        # Do the request and get the response data
        req = requests.get(OPENWEATHERMAP_URL, params=params)

        if req.status_code == 400:
            logging.warning('Bad request on getting the weather')
            data = {}
            data['msg'] = 'Bad request'
            data['code'] = 400
            return json.dumps(data)

        if req.status_code == 401:
            logging.warning('unauthorized - please check the APPID')
            data = {}
            data['msg'] = 'unauthorized'
            data['code'] = 401
            return json.dumps(data)

        res = req.json()

        # Use the first result
        result = res['main']

        return result
    except Exception as e:
        logging.error("An error ocurred while try to get the weather.")
        logging.error(e)


