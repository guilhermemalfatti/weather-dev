import requests
import json
import logging


def get_lat_long(address):
    APPID = 'sQ944lomntpZwae6Efdu'
    APPCODE = 'IY_pkrYwW2Dug3KGEMOghw'
    GEOCODE_HERE_URL = 'https://geocoder.api.here.com/6.2/geocode.json'


    logging.info("get location call.")
    logging.info(address)

    try:
        params = {
            'app_id': APPID,
            'app_code': APPCODE,
            'searchtext': address
        }

        # Do the request and get the response data
        req = requests.get(GEOCODE_HERE_URL, params=params)

        if req.status_code == 400:
            logging.warning('Bad request on getting the location')
            data = {}
            data['msg'] = 'Bad request'
            data['code'] = 400
            return json.dumps(data)

        if req.status_code == 401:
            logging.warning('unauthorized - please check the appid and appcode')
            data = {}
            data['msg'] = 'unauthorized'
            data['code'] = 401
            return json.dumps(data)

        res = req.json()

        if len(res['Response']['View']) == 0:
            logging.warning('There is no results')
            return res['Response']['View']

        # Use the first result
        result = res['Response']['View'][0]['Result'][0]['Location']
    except:
        logging.error("An error o ocurred while try to get the location.")


    return result

