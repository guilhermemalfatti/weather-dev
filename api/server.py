from flask import Flask, request
from flask_restful import Resource, Api
from ws.location import get_lat_long
from ws.weather import get_weather
from util.data import get_data, get_logs, create_historical_data
from flask_cors import CORS

import logging
logging.basicConfig(level=logging.INFO)


app = Flask(__name__)
CORS(app)
api = Api(app)


class Logs(Resource):
    def get(self):
        return get_logs()


class Weather(Resource):
    def get(self):
        SEARCH_TEXT = 'searchText'
        LOCATION = 'location'
        ADDRESS = 'Address'
        if request.args.get(SEARCH_TEXT):
            # if the data in database is recent, do not call external API
            temperature = get_data(request.args.get(SEARCH_TEXT))
            if temperature:
                logging.info("Data by cache")
                return temperature
            else:
                logging.info("Data by external API")
                location = get_lat_long(request.args.get(SEARCH_TEXT))
                if len(location) == 0:
                    return {}

                weather = get_weather(location['DisplayPosition'])
                create_historical_data(request.args.get(SEARCH_TEXT), location, weather['temp'])

                try:
                    weather[LOCATION] = location[ADDRESS]['City'] + ", "
                except:
                    logging.warning("location don't have city property")

                try:
                    weather[LOCATION] = weather[LOCATION] + location[ADDRESS]['State'] + " - "
                except:
                    logging.warning("location don't have State property")

                try:
                    weather[LOCATION] = weather[LOCATION] + location[ADDRESS]['Country']
                except:
                    logging.warning("location don't have Country property")

                return weather
        else:
            return "Query string searchText is required"


api.add_resource(Weather, '/weather')
api.add_resource(Logs, '/logs')

if __name__ == '__main__':
    logging.info('server listen on port 5002.')
    app.run(port='5002')
