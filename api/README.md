# API

The API provides a connection to the geocoder API and penweathermap api, that aims return the temperature to the given address.
The API works with SQLITE database, that store the all the searches, that aims to creat a historical data and also use as a cache.
The Project runs wth python, using Flask Framework.

### Project Requirements

| Requirement | Description
| ------ | ------
| [REQ-001](#python) |  The back end application should be developed using Python and any SQL database |
| [REQ-002](#api) |  Must use an API to get the zipcode for the given address |
| [REQ-003](#temp) | Assume the temperature for a Zipcode doesn’t vary within 1-hour window |
| [REQ-004](#api) | Must use Open Weather Map’s API in order to fetch the temperature for a certain
Zipcode |
| [REQ-005](#posts) | The code should be tested |


## Running Locally

Make sure you have [Python](https://www.python.org) and [Git](https://git-scm.com/) installed.


```sh
$ git clone https://guilhermemalfatti@bitbucket.org/guilhermemalfatti/weather-full-stack-dev.git  # or clone your own fork
$ cd weather-full-stack-dev/api
$ virtualenv  venv
$ source venv/Scripts/activate
$ pip install -r requirements.txt
$ python server.py
```

Your app should now be running on [localhost:5000](http://localhost:5000/).


## Tests
In order to run unit tests, follow the commands bellow:
```sh
$ git clone https://guilhermemalfatti@bitbucket.org/guilhermemalfatti/weather-full-stack-dev.git  # or clone your own fork
$ cd weather-full-stack-dev/api
$ pytest ./tests
```