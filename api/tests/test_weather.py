
import sys, os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')

from unittest import TestCase
from unittest.mock import patch, MagicMock
from ws.weather import get_weather

response = {
    "test": 1
}

values = {
    "Latitude": 123,
    "Longitude": 132465
}

def mock_request(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    return MockResponse({"main": {'test': 1}}, 200)


class TestWeather(TestCase):
    @patch('ws.weather.requests.get', side_effect=mock_request)
    def test_get_weather(self, mock_request):
        r = get_weather(values)

        self.assertEqual(r, response , "The return should be equal.")
