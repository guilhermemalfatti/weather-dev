import sys, os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')

from unittest import TestCase
from unittest.mock import patch, MagicMock
from ws.location import get_lat_long

response = 1

address = 'my fake addr'

def mock_request(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    return MockResponse({'Response': {'View': [{'Result': [{'Location': 1}]}] }}, 200)


class TestLocation(TestCase):
    @patch('ws.location.requests.get', side_effect=mock_request)
    def test_get_location(self, mock_request):
        r = get_lat_long(address)

        self.assertEqual(r, response, "The return should be equal.")
