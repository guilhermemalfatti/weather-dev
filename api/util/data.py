from database.persistent_db import Persistent_db
import logging


def create_historical_data(address, location, temperature):
    ADDRESS = 'Address'
    DISPLAY_POSITION = 'DisplayPosition'
    try:
        INSERT_QUERY = '''INSERT INTO historical_data (search, latitude, longitude, temp, location) VALUES (?, ?, ?, ?, ?);'''
        area = ''
        try:
            area = location[ADDRESS]['City'] + ", "
        except:
            logging.warning("location don't have city property")


        try:
            area = area + location[ADDRESS]['State'] + " - "
        except:
            logging.warning("location don't have State property")

        try:
            area = area + location[ADDRESS]['Country']
        except:
            logging.warning("location don't have Country property")

        params = (address, location[DISPLAY_POSITION]["Latitude"], location[DISPLAY_POSITION]["Longitude"], temperature, area)
        db = Persistent_db()
        db.execute(INSERT_QUERY, params)
        db.close()
    except Exception as e:
        logging.error("An error ocurred while try insert data on database.")
        logging.error(e)

def get_data(address):
    try:
        QUERY = '''select temp, location from historical_data
                    where
                    search = ? and
                    creation_date BETWEEN   DATETIME(DATETIME(), '-60 minutes') and DATETIME();'''

        params = (address,)
        db = Persistent_db()
        result = db.query(QUERY, params)
        db.close()
        return result
    except Exception as e:
        logging.error("An error ocurred while try get the temperature on the database.")
        logging.error(e)

def get_logs():
    try:
        QUERY = '''select id, search, datetime(creation_date, "localtime") AS creation_date, latitude, longitude, temp, location from historical_data
                    order by creation_date desc
                    limit 100;'''

        params = ()
        db = Persistent_db()
        result = db.query(QUERY, params, True)
        db.close()
        return result
    except Exception as e:
        logging.error("An error ocurred while try get logs.")
        logging.error(e)
