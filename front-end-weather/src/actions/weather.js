import axios from "axios";
import ActionCreator from './actionCreators';
import { API_ENDPOINT } from '../config/config';
import { receiveInitialData } from './logs';
import _ from 'lodash';


/**
 * Method responsible for return the teperature
 * @param {*} id The post id
 */
export function currentTemp(values) {
    return (dispatch) => {
        dispatch(ActionCreator.requestData());
        values.searchText = values.searchText.replace(/\s/g, '+')

        return axios.get(API_ENDPOINT.WEATHER_API + '/weather?searchText=' + values.searchText)
            .then((res) => {
                if(_.isEmpty(res.data)){
                    alert("Don't have any result, try another one!");
                }else{
                    dispatch(ActionCreator.showCurrentTemp(res.data));
                    dispatch(receiveInitialData());
                }

                dispatch(ActionCreator.dataReceived());
            })
            .catch((err) => {
                alert('There was an error on getting the temperature, the data is inconsistent, refresh and try again. ' + err)
            });
    }
}