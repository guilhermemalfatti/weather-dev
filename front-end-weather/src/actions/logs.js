import ActionCreator from './actionCreators';
import { API_ENDPOINT } from '../config/config';
import axios from "axios";

/**
 * Method responsible for return the initial data of the app, that means, all logs
 */
export function receiveInitialData() {
    return (dispatch) => {
        dispatch(ActionCreator.requestData());
        return axios.get(API_ENDPOINT.WEATHER_API + '/logs')
            .then((res) => {
                dispatch(ActionCreator.initialData(res.data));
                dispatch(ActionCreator.dataReceived());
            })
            .catch((err) => {
                console.error("Error on getting the data " + err)
            })
    }
}