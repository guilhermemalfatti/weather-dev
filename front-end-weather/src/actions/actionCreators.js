import { createActions } from 'reduxsauce'

export const { Types, Creators } = createActions({
  showCurrentTemp: ['weather'],

  initialData: ['logs'],

  requestData: null,
  dataReceived: null,

})
export default Creators
