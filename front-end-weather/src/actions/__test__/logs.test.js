import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moxios from 'moxios';
import expect from 'expect';
import { receiveInitialData } from '../logs';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Logs', () => {

    beforeEach(function () {
        moxios.install();
    });

    afterEach(function () {
        moxios.uninstall();
    });


    it('receiveInitialData', async () => {
        var expectResult = [];
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: expectResult,
            });
        });

        const store = mockStore();
        await store.dispatch(receiveInitialData());
        const actions = store.getActions();

        expect(actions[0]).toEqual({ type: "REQUEST_DATA"});
        expect(actions[1]).toEqual({ type: "INITIAL_DATA", logs: expectResult});
        expect(actions[2]).toEqual({ type: "DATA_RECEIVED"});

    });


});