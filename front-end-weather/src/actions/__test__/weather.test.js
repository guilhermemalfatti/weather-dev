import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moxios from 'moxios';
import expect from 'expect';
import { currentTemp } from '../weather';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Logs', () => {

    beforeEach(function () {
        moxios.install();
    });

    afterEach(function () {
        moxios.uninstall();
    });


    it('receiveInitialData', async () => {
        var expectResult = {test:1};
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: expectResult,
            });
        });

        const store = mockStore();
        await store.dispatch(currentTemp({searchText: 'test'}));
        const actions = store.getActions();

        expect(actions[0]).toEqual({ type: "REQUEST_DATA"});
        expect(actions[1]).toEqual({ type: "SHOW_CURRENT_TEMP", weather: expectResult});


        expect(actions[2]).toEqual({ type: "REQUEST_DATA"});
        expect(actions[3]).toEqual({ type: "DATA_RECEIVED"});

    });


});