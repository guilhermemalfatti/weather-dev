import React, { Component } from 'react';
import './App.css';
import Weather from './weather/weather'
import Logs from './logs/logs'
import Loading from './loading/index';
import { connect } from 'react-redux';
import { receiveInitialData } from '../actions/logs'

class App extends Component {

  componentDidMount() {
    const { onReceiveInitialData } = this.props

    onReceiveInitialData();
  }

  render() {
    let { isLoading } = this.props;

    if(isLoading){
      return(
        <Loading/>
      )
    }

    return (
      <div className="App">

        <Weather />
        <Logs />

      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onReceiveInitialData: () => {
      dispatch(receiveInitialData())
    }
  }
}

export default connect((state) => ({
  isLoading: state.loading.isLoading
}), mapDispatchToProps)(App)
