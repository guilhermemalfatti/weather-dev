import React, { Component } from 'react';
import './index.css';
import { connect } from 'react-redux';
import { Table } from 'react-bootstrap';

class Logs extends Component {

  render() {
    let { logs } = this.props;
    return (
      <div className="logs">
        <Table striped bordered condensed hover >
          <thead>
            <tr>
              <th>#</th>
              <th>Search Text</th>
              <th>Creation Date</th>
              <th>Temperature</th>
              <th>Location</th>
              <th>Latitude</th>
              <th>Longitude</th>
            </tr>
          </thead>
          <tbody>
            {logs && logs.map((item) => (
              <tr>
                <td>{item.id}</td>
                <td>{item.search}</td>
                <td>{item.creation_date}</td>
                <td>{item.temp}   &#8451;</td>
                <td>{item.location}</td>
                <td>{item.latitude}</td>
                <td>{item.longitude}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    )
  }
}




export default connect((state) => ({
  logs: state.logs.items
}))(Logs)
