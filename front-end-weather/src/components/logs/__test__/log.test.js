import React from 'react';
import Logs from '../logs';
import renderer from 'react-test-renderer';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';


const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

test('Logs render snapshot', () => {
    const store = mockStore({
        logs: []
    });

    const component = renderer.create(
        <Provider store={store}><Logs /></Provider>
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();

});