import React, { Component } from 'react';
import './index.css';
import { connect } from 'react-redux';
import serializeForm from 'form-serialize';
import { currentTemp } from '../../actions/weather';
import _ from 'lodash';

class Weather extends Component {

    state = {
        searchText: ''
    };

    /**
    * Method responsible for handle the form submit
    * @param {*} e The event
    */
    handleSubmit = (e) => {
        let { onShowCurrentTemp } = this.props;
        e.preventDefault()
        const values = serializeForm(e.target, { hash: true });

        onShowCurrentTemp(values);

        this.clearForm();
    }

    /**
     * Method responsible for clear the form
     */
    clearForm = ()=> {

        this.setState({
            searchText: ''
        });

    }

     /**
     * @description Handle the input component value change
     * @param {object} event - The event
     */
    handleInputChange(event) {
        this.setState({ searchText: event.target.value });
    }

    render() {
        let { searchText } = this.state;
        let { weather } = this.props;

        const disabled = _.isEmpty(searchText);
        return (
            <div className="container">
                <h2>Search the temperature</h2>
                <form onSubmit={this.handleSubmit} >
                    <div className="control">
                        <input name="searchText" value={searchText} onChange={(event) => this.handleInputChange(event)} type="text" placeholder="Enter the address" />
                        <button disabled={disabled} type="submit" id="btn">Search</button>
                    </div>
                </form>
                <p>Temperature: <span className="temperature">{weather.temp} &#8451;</span></p>
                <p>Location: <span className="city_country">{weather.location} </span></p>
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        onShowCurrentTemp: values => {
            dispatch(currentTemp(values));
        }
    }
}

export default connect((state) => ({
    weather: state.weather
}), mapDispatchToProps)(Weather)
