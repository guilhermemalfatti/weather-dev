import React from 'react';
import Weather from '../weather';
import renderer from 'react-test-renderer';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';


const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

test('Weather render snapshot', () => {
    const store = mockStore({
        weather: {},
        searchText: ''
    });

    const component = renderer.create(
        <Provider store={store}><Weather /></Provider>
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();

});