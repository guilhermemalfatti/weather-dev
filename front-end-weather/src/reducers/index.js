import { combineReducers } from 'redux';

import logs from './logs';
import weather from './weather';
import loading from './loading';

export default combineReducers({
    logs,
    weather,
    loading
})