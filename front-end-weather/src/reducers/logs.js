import { createReducer } from 'reduxsauce'
import { Types } from '../actions/actionCreators'

const INITIAL_STATE = {
    items: []
};

const intialdata = (state = INITIAL_STATE, action) => {
    return {
      ...state,
      items: action.logs
  }
}

const HANDLERS = {
    [Types.INITIAL_DATA]: intialdata
}

export default createReducer(INITIAL_STATE, HANDLERS)
