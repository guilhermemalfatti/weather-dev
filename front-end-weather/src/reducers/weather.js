import { createReducer } from 'reduxsauce'
import { Types } from '../actions/actionCreators'

const INITIAL_STATE = {
  temp: "-",
  location: "N/A"
};

const showCurrentTemp = (state = INITIAL_STATE, action) => {
  return {
    ...state,
    temp: action.weather.temp,
    location: action.weather.location
  }
}



const HANDLERS = {
  [Types.SHOW_CURRENT_TEMP]: showCurrentTemp
}

export default createReducer(INITIAL_STATE, HANDLERS)
