## Wether app

Available on: https://front-temperature.herokuapp.com/

obs: Since the application is unused for a while, its get unloaded from the server memory, so the first time that you will open the app wil take a little longer.


The goal is to create an application that allows users to see the current temperature at a certain address.


### Project Requirements

| Requirement | Description
| ------ | ------
| [REQ-001](#UI) |  The UI should be written using HTML, JavaScript and CSS (you may use React, Vue.js, Angular) |
| [REQ-002](#test) |  The code should be tested |

## The following libraries are used in this project:

- **react**: [React](https://reactjs.org/)
- **lodash**: [Lodash](https://lodash.com/)
- **redux**: [Redux](https://redux.js.org/)

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) and [Git](https://git-scm.com/) installed.

Follow the instructions to start the [API server](https://github.com/udacity/reactnd-project-readable-starter) locally.

```sh
$ git clone https://guilhermemalfatti@bitbucket.org/guilhermemalfatti/weather-full-stack-dev.git  # or clone your own fork
$ cd weather-full-stack-dev/front-end-weather
$ npm install
$ npm start
```

Your app should now be running on [localhost:3000](http://localhost:3000/).

## Tests
In order to run unit tests, follow the commands bellow:

```sh
$ git clone https://guilhermemalfatti@bitbucket.org/guilhermemalfatti/weather-full-stack-dev.git  # or clone your own fork
$ cd weather-full-stack-dev/front-end-weather
$ npm test
```

## References

For more information about using Node.js and React, see these Dev Center articles:

- [Getting Started with Node.js on Heroku](https://devcenter.heroku.com/articles/getting-started-with-nodejs)
- [React, a JavaScript library for building user interfaces](https://reactjs.org/tutorial/tutorial.html)
- [react-redux](https://redux.js.org/basics/usagewithreact)

## TODO
- Mechanism of error message
- Unit Tests (coverage > 80%)
