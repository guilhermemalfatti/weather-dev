# Weather app

The goal is to create an application that allows users to see the current temperature at a
certain address

This repository includes the code for the backend API Server and the front-end portion of the project.

## Starting

To get started developing right away:

* Install and start the API server
    - `virtualenv  venv`
    - `source venv/Scripts/activate`
    - `pip install -r requirements.txt`
    - `python server.py`
* In another terminal window, use Create React App to scaffold out the front-end
    - `cd front-end-weather`
    - `npm install`
    - `npm start`

## API Server

Information about the API server and how to use it can be found in its [README file](api-server/README.md).